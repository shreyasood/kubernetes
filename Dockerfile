From python

WORKDIR /Desktop/Flask_with_mongo_multi_container

COPY . /Desktop/Flask_with_mongo_multi_container

RUN pip install flask 

RUN pip install pymongo

EXPOSE 9000

CMD ["python3","mongo2.py"]