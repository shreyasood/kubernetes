from flask import Flask, request
import json
from pymongo import MongoClient

client=MongoClient('mongoservice') #for docker:-('mongo', 27017) as written in docker-compose.yaml file in image:mongo
#client=MongoClient('mongo',27017)
db = client.mydatabase
col = db.mycollection
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST', 'PUT','DELETE'])
def get():
    if request.method=='GET':
        val = col.find()
        l=[]
        for i in val:
            l.append(i)
        return (str(l))
    elif request.method=='POST':
        va1=request.get_json()
        col.insert_one(va1)
        return 'post done'
    elif request.method=='PUT':
        va2=request.get_json()
        col.update_one(va2[0],{"$set":va2[1]})
        return 'put done'
    elif request.method=='DELETE':
        va3=request.get_json()
        col.delete_one(va3)
        return 'delete done'



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port =9000)